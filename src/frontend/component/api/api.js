
// importing axios
import axios from 'axios';

axios.defaults.baseURL = 'https://minilista.herokuapp.com';
let apiUrl = 'https://minilista.herokuapp.com'

// axios.defaults.baseURL = 'https://localhost:3000';
// let apiUrl = 'http://localhost:3000'

/**
* API class, responsible for handling parameters and functions to connect and handle data
necessary to firebase.
*/
export default class API {

    getBase(base) {
        return new Promise((resolve, reject) => {

            let configOptions = {
                method: 'GET',
                url: base ? apiUrl + '/carregaBase?base=' + base : '/carregaBase',
                headers: {
                    'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1OTNiNWFjODczMmVlMTJjNDE0NGI1MTgiLCJ0aXR1bG8iOiJQZWRybyBMaW1hIiwicnVhIjoiQXZlbmlkYSBDb3JvbmVsIEpvw6NvIEZlcm5hbmRlcyIsIm51bWVybyI6IjE2NiIsImNvbXBsZW1lbnRvIjoiIiwiYmFpcnJvIjoiQ2VudHJvIiwiY2lkYWRlIjoiQXJhcmFuZ3XDoSIsInVmIjoiU0MiLCJjZXAiOiI4ODkwMDAwMCIsInNpdGUiOiIiLCJlbWFpbCI6IiIsImRlc2NyaWNhbyI6IiIsImZhY2Vib29rSWQiOiIxMDAxMTAxMjAzIiwiaW1hZ2VtIjpudWxsLCJhdGl2byI6dHJ1ZSwiYmFzZSI6IkFyYXJhbmd1w6EtU0MiLCJfX3YiOjAsInRlbGVmb25lcyI6W3sibnVtZXJvIjoiKDMxKSA5MjUwIDE0NjUiLCJvcGVyYWRvcmEiOiJ0aW0ifV0sImlhdCI6MTQ5NzA2MjU1MCwiZXhwIjoxNTI4NjIwMTUwfQ.f9Pbm1RBpCxq-MDty3qDgacIDUUqnq9JIOI_gnH7i6M',
                    'Content-Type': 'application/json'
                }
            }

            /* post to api using the configuration */
            axios(configOptions).then((response) => {
                resolve(response.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }


    getBaseNomes() {
        return new Promise((resolve, reject) => {

            let configOptions = {
                method: 'GET',
                url: apiUrl + '/carregaBaseNomes',
                headers: {
                    'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1OTNiNWFjODczMmVlMTJjNDE0NGI1MTgiLCJ0aXR1bG8iOiJQZWRybyBMaW1hIiwicnVhIjoiQXZlbmlkYSBDb3JvbmVsIEpvw6NvIEZlcm5hbmRlcyIsIm51bWVybyI6IjE2NiIsImNvbXBsZW1lbnRvIjoiIiwiYmFpcnJvIjoiQ2VudHJvIiwiY2lkYWRlIjoiQXJhcmFuZ3XDoSIsInVmIjoiU0MiLCJjZXAiOiI4ODkwMDAwMCIsInNpdGUiOiIiLCJlbWFpbCI6IiIsImRlc2NyaWNhbyI6IiIsImZhY2Vib29rSWQiOiIxMDAxMTAxMjAzIiwiaW1hZ2VtIjpudWxsLCJhdGl2byI6dHJ1ZSwiYmFzZSI6IkFyYXJhbmd1w6EtU0MiLCJfX3YiOjAsInRlbGVmb25lcyI6W3sibnVtZXJvIjoiKDMxKSA5MjUwIDE0NjUiLCJvcGVyYWRvcmEiOiJ0aW0ifV0sImlhdCI6MTQ5NzA2MjU1MCwiZXhwIjoxNTI4NjIwMTUwfQ.f9Pbm1RBpCxq-MDty3qDgacIDUUqnq9JIOI_gnH7i6M',
                    'Content-Type': 'application/json'
                }
            }

            /* post to api using the configuration */
            axios(configOptions).then((response) => {
                resolve(response.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    atualizaUser(user) {
        return new Promise((resolve, reject) => {

            let configOptions = {
                method: 'PUT',
                url: apiUrl + '/atualizaUser',
                data: JSON.stringify(user),
                headers: {
                    'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1OTNiNWFjODczMmVlMTJjNDE0NGI1MTgiLCJ0aXR1bG8iOiJQZWRybyBMaW1hIiwicnVhIjoiQXZlbmlkYSBDb3JvbmVsIEpvw6NvIEZlcm5hbmRlcyIsIm51bWVybyI6IjE2NiIsImNvbXBsZW1lbnRvIjoiIiwiYmFpcnJvIjoiQ2VudHJvIiwiY2lkYWRlIjoiQXJhcmFuZ3XDoSIsInVmIjoiU0MiLCJjZXAiOiI4ODkwMDAwMCIsInNpdGUiOiIiLCJlbWFpbCI6IiIsImRlc2NyaWNhbyI6IiIsImZhY2Vib29rSWQiOiIxMDAxMTAxMjAzIiwiaW1hZ2VtIjpudWxsLCJhdGl2byI6dHJ1ZSwiYmFzZSI6IkFyYXJhbmd1w6EtU0MiLCJfX3YiOjAsInRlbGVmb25lcyI6W3sibnVtZXJvIjoiKDMxKSA5MjUwIDE0NjUiLCJvcGVyYWRvcmEiOiJ0aW0ifV0sImlhdCI6MTQ5NzA2MjU1MCwiZXhwIjoxNTI4NjIwMTUwfQ.f9Pbm1RBpCxq-MDty3qDgacIDUUqnq9JIOI_gnH7i6M',
                    'Content-Type': 'application/json'
                }
            }

            /* post to api using the configuration */
            axios(configOptions).then((response) => {
                console.log(response);
                resolve(response.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

    deletaUser(user) {
        return new Promise((resolve, reject) => {

            let configOptions = {
                method: 'DELETE',
                url: apiUrl + '/deletaUser',
                data: JSON.stringify(user),
                headers: {
                    'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1OTNiNWFjODczMmVlMTJjNDE0NGI1MTgiLCJ0aXR1bG8iOiJQZWRybyBMaW1hIiwicnVhIjoiQXZlbmlkYSBDb3JvbmVsIEpvw6NvIEZlcm5hbmRlcyIsIm51bWVybyI6IjE2NiIsImNvbXBsZW1lbnRvIjoiIiwiYmFpcnJvIjoiQ2VudHJvIiwiY2lkYWRlIjoiQXJhcmFuZ3XDoSIsInVmIjoiU0MiLCJjZXAiOiI4ODkwMDAwMCIsInNpdGUiOiIiLCJlbWFpbCI6IiIsImRlc2NyaWNhbyI6IiIsImZhY2Vib29rSWQiOiIxMDAxMTAxMjAzIiwiaW1hZ2VtIjpudWxsLCJhdGl2byI6dHJ1ZSwiYmFzZSI6IkFyYXJhbmd1w6EtU0MiLCJfX3YiOjAsInRlbGVmb25lcyI6W3sibnVtZXJvIjoiKDMxKSA5MjUwIDE0NjUiLCJvcGVyYWRvcmEiOiJ0aW0ifV0sImlhdCI6MTQ5NzA2MjU1MCwiZXhwIjoxNTI4NjIwMTUwfQ.f9Pbm1RBpCxq-MDty3qDgacIDUUqnq9JIOI_gnH7i6M',
                    'Content-Type': 'application/json'
                }
            }

            /* post to api using the configuration */
            axios(configOptions).then((response) => {
                console.log(response);
                resolve(response.data)
            }).catch((error) => {
                reject(error)
            })
        })
    }

}

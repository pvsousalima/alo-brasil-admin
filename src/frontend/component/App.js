import React, {Component} from 'react'

import AppBar from 'react-toolbox/lib/app_bar'
import Navigation from 'react-toolbox/lib/navigation'
import Dialog from 'react-toolbox/lib/dialog'
import Input from 'react-toolbox/lib/input'

import { Button } from 'react-toolbox/lib/button' // Bundled component import
import {Tab, Tabs} from 'react-toolbox'
import { List, ListItem, ListSubHeader, ListDivider, ListCheckbox } from 'react-toolbox/lib/list'
import ProgressBar from 'react-toolbox/lib/progress_bar'
import Checkbox from 'react-toolbox/lib/checkbox'
import Modal from 'react-modal'
import { Snackbar } from 'react-toolbox'
import ReactList from 'react-list';

import faker from 'faker'
import shortid from 'shortid'

import Dropdown from 'react-toolbox/lib/dropdown'

import BaseList from './BaseList.js'
import {API} from './api'
global.api = new API



const RightSide = () => {
    return(
        <div style={{alignItems:'flex-start', justifyContent:'center'}}>
            <h5>
                Isso e um texto
            </h5>
        </div>
    )
}

class App extends Component {

    state = {
        index: 0,
    }

    handleTabChange = (index) => {
        this.setState({index})
    }

    render() {
        return (
            <div className="App">

                <AppBar>

                    {/* <AppBar rightIcon={<RightSide/>}> */}
                    {/* <img style={{width:50, height:50}} src={logo} className="App-logo" alt="logo" /> */}
                    <h2>Administração Alô Brasil App</h2>
                </AppBar>

                <Tabs index={this.state.index} fixed={true} onChange={this.handleTabChange}>

                    <Tab label='Ver usuários'  ripple={false}>
                        <section>
                            <UserList props={this.props} edit={this.handleToggle}/>
                        </section>
                    </Tab>

                    <Tab label='Ver Bases'  ripple={false}>
                        <section>
                            <BaseList props={this.props} edit={this.handleToggle}/>
                        </section>
                    </Tab>

                </Tabs>

            </div>

        )
    }
}


class UserList extends Component {

    constructor(props){
        super(props)
        this.openModal = this.openModal.bind(this)
        this.closeModal = this.closeModal.bind(this)

    }

    // states and cons
    state = {
        modalIsOpen: false,
        loading: true,
        minilista: [],
        user: {},
        active:false
    }

    tiposAnunciantes = [
        { value: 1, label: 'Ouro' },
        { value: 2, label: 'Prata'},
        { value: 3, label: 'Bronze' }
    ]

    // handlers
    handleChange = (name, value) => {
        let newuser = this.state.user
        newuser[name] = value
        this.setState({user: newuser})
    }

    openModal() {
        this.setState({modalIsOpen: true})
    }

    closeModal() {
        this.setState({modalIsOpen: false})
    }

    openSnackBar(event, instance) {
        this.setState({ active: true })
    }

    handleSnackbarTimeout(event, instance) {
        this.setState({ active: false })
    }

    handleSnackbarClick(event, instance) {
        this.setState({ active: false })
    }

    componentWillMount(){
        this.refreshData()
    }


    refreshData(){
        api.getBase().then((minilista) => {
            this.setState({minilista: minilista, loading:false})
        })
    }

    atualizaUser(){
        return api.atualizaUser(this.state.user)
    }

    deletaUser(){
        return api.deletaUser(this.state.user)
    }

    renderList(){
        return this.state.minilista.map((item) => {
            return this.renderItem(item)
        })
    }

    renderItem(index, key){
        // console.log(index, item);
        return(
            <div>
                <ListItem
                    key={key}
                    ripple={false}
                    selectable
                    avatar={this.state.minilista[index].imagem || 'https://cdn.worldvectorlogo.com/logos/react-1.svg'}
                    caption={this.state.minilista[index].titulo}
                    legend={this.state.minilista[index].base}
                    onClick={() => {
                        this.openModal()
                        this.setState({user: this.state.minilista[index]})
                    }}/>
                    <ListDivider/>
                </div>
            )
        }


        renderSnackBar(){
            return(
                <Snackbar
                    action='Fechar'
                    active={this.state.active}
                    label='Dados salvos!'
                    timeout={2000}
                    onClick={this.handleSnackbarClick.bind(this)}
                    onTimeout={this.handleSnackbarTimeout.bind(this)}
                    type='accept'
                />
            )
        }

        renderModal(){
            return(
                <Modal isOpen={this.state.modalIsOpen} onRequestClose={this.closeModal} contentLabel="Editar usuário">

                    <Input type='text' label='Título' name='name' value={this.state.user.titulo} onChange={this.handleChange.bind(this, 'titulo')}/>
                    <Input type='text' label='Endereço' name='endereco' value={this.state.user.endereco} onChange={this.handleChange.bind(this, 'endereco')} />
                    <Input type='text' label='Cidade' name='cidade' value={this.state.user.cidade} onChange={this.handleChange.bind(this, 'cidade')} />
                    <Input type='text' label='CEP' name='cep' value={this.state.user.cep} onChange={this.handleChange.bind(this, 'cep')} />
                    <Input type='text' label='Website' name='website' value={this.state.user.site} onChange={this.handleChange.bind(this, 'site')} />
                    <Input type='email' label='Email'  name='email' value={this.state.user.email} onChange={this.handleChange.bind(this, 'email')} />
                    <Input type='text' label='Descrição' name='descricao' value={this.state.user.descricao} onChange={this.handleChange.bind(this, 'descricao')} />
                    <Input type='text' label='FacebookId'  name='facebookId' value={this.state.user.facebookId} onChange={this.handleChange.bind(this, 'facebookId')} />
                    <Input type='text' label='Base' name='base' value={this.state.user.base} onChange={this.handleChange.bind(this, 'base')} />

                    <Dropdown
                        auto
                        label='Tipo do anunciante'
                        template={this.customItem}
                        onChange={this.handleChange.bind(this, 'tipoAnunciante')}
                        source={this.tiposAnunciantes}
                        value={this.state.user.tipoAnunciante}
                    />

                    <Checkbox
                        checked={this.state.user.ativo}
                        label="Usuário ativo"
                        onChange={() => {
                            let newuser = this.state.user
                            newuser.ativo = !newuser.ativo
                            this.setState({user: newuser})
                        }}/>
                    <Checkbox
                        checked={this.state.user.anunciante}
                        label="Usuário anunciante"
                        onChange={() => {
                                let newuser = this.state.user
                                newuser.anunciante = !newuser.anunciante
                            this.setState({user: newuser})
                        }}>
                        <div style={{paddingTop:30, paddingBottom:30}}>
                            <Button label='Salvar alterações' raised onClick={()=>{
                                this.atualizaUser().then(() => {
                                    this.closeModal()
                                    this.refreshData()
                                    this.openSnackBar()
                                })
                            }} />
                            <Button style={{marginLeft:15}} onClick={() => {
                                this.closeModal()
                            }}  label='Cancelar alterações' raised />

                            <Button style={{marginLeft:15, backgroundColor:'rgb(255,0,0)'}} onClick={() => {
                                this.deletaUser().then(() => {
                                    this.closeModal()
                                    this.refreshData()
                                    this.openSnackBar()
                                    })
                                }}  label='Excluir usuário'  raised />
                            </div>

                        </Checkbox>

                    </Modal>
                )
            }

            render(){
                return(
                    this.state.loading ?
                    (<ProgressBar type="linear" mode="indeterminate"/>) :
                    (<List>
                        <ListSubHeader caption='Usuários cadastrados'/>
                        <ListDivider/>
                        {/* {this.renderList()} */}

                        <ReactList
                            itemRenderer={this.renderItem.bind(this)}
                            length={this.state.minilista.length}
                            type='uniform'
                        />

                        {this.renderModal()}
                        {this.renderSnackBar()}
                        <div style={{position:'absolute', right:0, bottom:10}} >
                            <Button icon='add' floating  />
                        </div>
                    </List>

                )
            )
        }

    }


    export default App

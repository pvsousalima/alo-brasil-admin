import React, {Component} from 'react'

import AppBar from 'react-toolbox/lib/app_bar'
import Navigation from 'react-toolbox/lib/navigation'
import Dialog from 'react-toolbox/lib/dialog'
import Input from 'react-toolbox/lib/input'

import { Button } from 'react-toolbox/lib/button' // Bundled component import
import {Tab, Tabs} from 'react-toolbox'
import { List, ListItem, ListSubHeader, ListDivider, ListCheckbox } from 'react-toolbox/lib/list'
import ProgressBar from 'react-toolbox/lib/progress_bar'
import Checkbox from 'react-toolbox/lib/checkbox'
import Modal from 'react-modal'

import faker from 'faker'
import shortid from 'shortid'

class BaseList extends Component {

    constructor(props){
        super(props)
    }

    state = {
        loading: true,
        basenomes: [],
    }

    componentWillMount(){
        api.getBaseNomes().then((basenomes) => {
            this.setState({basenomes: basenomes, loading:false})
        })
    }

    renderList(){
        return this.state.basenomes.map((item) => {
            return this.renderItem(item)
        })
    }

    renderItem(item){
        return(
            <div>
                <ListItem
                    key={shortid.generate()}
                    ripple={false}
                    // selectable
                    // avatar={item.imagem || 'https://cdn.worldvectorlogo.com/logos/react-1.svg'}
                    caption={item}
                    // legend={item.base}
                    // rightIcon='delete'
                    itemAction={() => {
                        console.log('fui clickakllksdalk')
                    }}
                    onClick={() => {
                        this.handleToggle()
                        // this.setState({user: item})
                    }}/>
                    <ListDivider/>
                </div>
            )
        }

        render(){
            return(
                this.state.loading ?
                (<ProgressBar type="linear" mode="indeterminate"/>) :
                (<List>
                    <ListSubHeader caption='Bases cadastradas'/>
                    <ListDivider/>
                    {this.renderList()}
                </List>)
            )
        }

    }

    export default BaseList
